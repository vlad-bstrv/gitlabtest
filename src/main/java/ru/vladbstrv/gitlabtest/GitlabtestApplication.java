package ru.vladbstrv.gitlabtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class GitlabtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabtestApplication.class, args);
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        Person spongeBob = context.getBean("spongeBob", Person.class);
        Person patrick = context.getBean("patrick", Person.class);
        Person squidward = context.getBean("squidward", Person.class);

        System.out.println(spongeBob);
        System.out.println(patrick);
        System.out.println(squidward);
    }

}
